{{ name }}
==========

An LFE library

## Build
```
$ rebar3 compile
```

## REPL
```
$ rebar3 shell
```

## Test
```
$ rebar3 test
```
