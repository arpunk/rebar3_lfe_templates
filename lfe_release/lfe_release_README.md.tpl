{{ name }}
==========

An LFE application

## Build
```
$ rebar3 compile
```

## Test
```
$ rebar3 test
```

## REPL
```
$ rebar3 shell
```

## Run a release
```
$ rebar3 run
```
